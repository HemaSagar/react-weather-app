import React from 'react';
import './App.css';
import City from './Components/City';
import CurerentWeather from './Components/today-weather/today-weather';
import ForeCast from './Components/forecast/forecast';
import { WEATHER_API_URL,WEATHER_API_KEY } from './api';
import Loader from './Components/Loader';


class App extends React.Component {
  constructor() {
    super();
    

    this.state={
      city:null,
      currentData:null,
      loading:false,
      error:null
    }
  }
  oncityChange = (city) => {
    this.setState({loading:true})
    const currentWeather = fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=b71a2300404a6a3541fbf75dbf201c31&units=metric`);
    currentWeather.then((response)=> 
       response.json()
    )
    .then((data)=>{
      this.setState({city:city,currentData:data,loading:false}); 
    })
    .catch((err)=>{
      // console.log("error occurred",err);
      // this.setState({error:"hi",loading:false});
    })
  }

  render() {
    const obj = {city:this.state.city,weather:this.state.currentData,loading:this.state.loading};
    return (
      <div className='app-div'>
        <header>
        <h1>Weather App</h1>
      </header>
      <div className="container">
        
        <div className='aside'>
          <City  onSearchChange={this.oncityChange}/>
          <CurerentWeather data={obj} />          
        </div>
        {/* <ForeCast /> */}
      </div>
      </div>
    );
  }
}

export default App;
