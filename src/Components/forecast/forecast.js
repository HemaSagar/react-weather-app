import React from "react";
import "./forecast.css"
class ForeCast extends React.Component {
    render () {
        return (
            <div className="forecast">
                <div>
                    <h1 className="timespan">Daily</h1>
                </div>

                <div className="days-container">
                <div className="days">
                    <p className="weekday">Mon</p>
                    <img src="./images/02d.png" />
                    <div className="temp-min-max">
                        <span>35°C</span>
                        <span>27°C</span>
                    </div>
                </div>
                <div className="days">
                    <p className="weekday">Mon</p>
                    <img src="./images/02d.png" />
                    <div className="temp-min-max">
                        <span>35°C</span>
                        <span>27°C</span>
                    </div>
                </div>
                <div className="days">
                    <p className="weekday">Mon</p>
                    <img src="./images/02d.png" />
                    <div className="temp-min-max">
                        <span>35°C</span>
                        <span>27°C</span>
                    </div>
                </div>
                <div className="days">
                    <p className="weekday">Mon</p>
                    <img src="./images/02d.png" />
                    <div className="temp-min-max">
                        <span>35°C</span>
                        <span>27°C</span>
                    </div>
                </div>
                <div className="days">
                    <p className="weekday">Mon</p>
                    <img src="./images/02d.png" />
                    <div className="temp-min-max">
                        <span>35°C</span>
                        <span>27°C</span>
                    </div>
                </div>
                </div>
                
            </div>
        );
    }
}

export default ForeCast;