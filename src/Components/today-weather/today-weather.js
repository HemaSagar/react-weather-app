import React from "react";
import "./today-weather.css"
import Loader from "../Loader"

class CurerentWeather extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        let comp = "";
        if (this.props.data.loading) {
            console.log("loader")
            comp = <div className="today-weather">
                <Loader />
            </div>
        }
        else if (this.props.data.city === null) {
            console.log("city null")

            comp = <div className="today-weather">
                <h3 style={{textAlign:"center"}}>Please select a city to search</h3>
            </div>
        }
        else {
            const weather = this.props.data.weather;
            const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            const date = new Date(weather.dt * 1000);
            const displayDate = `${weekdays[date.getDay()]}, ${months[date.getMonth()]} ${date.getDate()}`;
            comp = <div className="today-weather">

                <h1 id="city-name">{this.props.data.city}</h1>
                <div className="weather-image">
                    <img src={`http://openweathermap.org/img/wn/${weather.weather["0"].icon}@2x.png`}></img>
                </div>
                <div className="temparature-div">
                    <p id="temperature">{weather.main.temp}°C</p>
                    <p id="description">{weather.weather["0"].main}</p>
                </div>
                <p id="date">{displayDate}</p>
                <div className="extra-details">

                    <div className="details">
                        <p>Max Temperature</p>
                        <p>{weather.main.temp_max}°C</p>
                    </div>
                    <div className="details">
                        <p>Min Temperature</p>
                        <p>{weather.main.temp_min}°C</p>
                    </div>
                    <div className="details">
                        <p>Visibility</p>
                        <p>{weather.visibility}</p>
                    </div>
                    <div className="details">
                        <p>Feels Like</p>
                        <p>{weather.main.feels_like}°C</p>
                    </div>

                </div>
            </div>
        }
        return comp;
    }
}

export default CurerentWeather;