import React from "react";
import { ReactDOM } from "react";

class City extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            city: ''
        }
    }
    handleCityChange = (event)  => {
        let {value} = event.target;
        this.setState({city:value});
        // console.log(this.state.city);
        this.props.onSearchChange(value);

    }
    render () {
        return (
            <select id="select-city" value={this.state.city} onChange={(e) => this.handleCityChange(e)}>
                <option disabled={true} value="" >Select a city</option>
                <option value="Bangalore" >Bangalore</option>
                <option value="Hyderabad" >Hyderabad</option>
                <option value="Delhi">Delhi</option>
            </select>
        );
    }
}

export default City;